package ro.dutylabs.mpp.core.repository;

import ro.dutylabs.mpp.core.model.Pizza;


public interface PizzaRepository extends BaseRepository<Pizza, Long> {
}
