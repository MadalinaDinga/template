package ro.dutylabs.mpp.web.dto;

import lombok.*;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class FilterDto implements Serializable {
    private String filterType;
    private String filterValue;
}
