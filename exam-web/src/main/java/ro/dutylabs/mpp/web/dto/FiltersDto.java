package ro.dutylabs.mpp.web.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class FiltersDto implements Serializable {
    private Set<FilterDto> filters;
}
