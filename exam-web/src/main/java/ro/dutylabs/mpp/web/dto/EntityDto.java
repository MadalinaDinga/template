package ro.dutylabs.mpp.web.dto;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class EntityDto extends BaseDto {
    private String name;
}
